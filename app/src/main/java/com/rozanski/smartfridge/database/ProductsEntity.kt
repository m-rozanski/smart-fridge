package com.rozanski.smartfridge.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.rozanski.smartfridge.PRODUCTS_TABLE_NAME

@Entity(tableName = PRODUCTS_TABLE_NAME)
data class ProductsEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "date") val date: Long,
    @ColumnInfo(name = "quantity") val quantity: Double,
    @ColumnInfo(name = "unit") val unit: String
)
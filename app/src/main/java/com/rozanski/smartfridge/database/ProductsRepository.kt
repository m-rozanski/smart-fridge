package com.rozanski.smartfridge.database

import android.util.Log
import com.rozanski.smartfridge.di.component.DaggerProductsRepositoryComponent
import com.rozanski.smartfridge.di.module.DatabaseModule
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

class ProductsRepository {
    @Inject
    lateinit var database: ProductsDatabase
    private lateinit var dao: ProductsDao

    fun setup() {
        DaggerProductsRepositoryComponent
            .builder()
            .databaseModule(DatabaseModule)
            .build()
            .inject(this)
        dao = database.productsDao()
    }

    fun getAll(): Flowable<List<ProductsEntity>> {
        Log.d("My", "Database: getAll")
        return dao.getAll()
    }

    fun insert(productsEntity: ProductsEntity): Completable {
        Log.d("My", "Database: insert")
        return dao.insert(productsEntity)
    }

    fun delete(productsEntity: ProductsEntity): Completable {
        Log.d("My", "Database: delete")
        return dao.delete(productsEntity)
    }

    fun update(productsEntity: ProductsEntity): Completable {
        Log.d("My", "Database: delete")
        return dao.update(productsEntity)
    }
}
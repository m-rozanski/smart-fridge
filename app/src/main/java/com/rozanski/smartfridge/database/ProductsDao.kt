package com.rozanski.smartfridge.database

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface ProductsDao {
    @Query("SELECT * FROM products ORDER BY date")
    fun getAll(): Flowable<List<ProductsEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(productsEntity: ProductsEntity): Completable

    @Delete
    fun delete(productsEntity: ProductsEntity): Completable

    @Update
    fun update(productsEntity: ProductsEntity): Completable
}
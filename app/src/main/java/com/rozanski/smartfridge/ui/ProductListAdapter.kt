package com.rozanski.smartfridge.ui

import android.graphics.Typeface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rozanski.smartfridge.R
import com.rozanski.smartfridge.database.ProductsEntity
import com.rozanski.smartfridge.decimalFormat
import com.rozanski.smartfridge.long2Date
import kotlinx.android.synthetic.main.recyclerview_product.view.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class ProductListAdapter(
    var items: ArrayList<ProductsEntity>,
    private val listener: MyClickListener
) : RecyclerView.Adapter<ProductListAdapter.ProductListViewHolder>() {

    val calendar = Calendar.getInstance()
    lateinit var dateFormat: SimpleDateFormat

    interface MyClickListener {
        fun handleItemClicked(productsEntity: ProductsEntity)
        fun handleItemLongClicked(productsEntity: ProductsEntity)
    }

    inner class ProductListViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        private var textView_name: TextView
        private var textView_date: TextView
        private var textView_q_u: TextView


        init {
            textView_name = view.textView_name
            textView_date = view.textView_date
            textView_q_u = view.textView_q_u
        }

        fun bind(item: ProductsEntity) {
            val date = long2Date(item.date)
            val diffInDays = TimeUnit.MILLISECONDS.toDays(date.time - calendar.timeInMillis)

            textView_name.text = item.name
            dateFormat = SimpleDateFormat(view.context.resources.getString(R.string.dateFormat))
            Log.d("My", "Adpater: ${item.name} - $diffInDays")

            if (diffInDays > 7) {
                textView_date.text = dateFormat.format(date)
                textView_date.setTextColor(view.context.resources.getColor(android.R.color.secondary_text_light))
                textView_date.setTypeface(null, Typeface.NORMAL)
                Log.d("My", "Adpater: ${item.name} ustawiono warunek 1")
            }
            else if (diffInDays > 1) {
                textView_date.text =
                    "${view.context.resources.getString(R.string.daysToExpire)}: $diffInDays"
                textView_date.setTextColor(view.context.resources.getColor(R.color.colorTextOrange))
                textView_date.setTypeface(null, Typeface.NORMAL)
                Log.d("My", "Adpater: ${item.name} ustawiono warunek 2")
            } else {
                textView_date.text =
                    "${view.context.resources.getString(R.string.daysToExpire)}: $diffInDays"
                textView_date.setTextColor(view.context.resources.getColor(R.color.colorTextRed))
                textView_date.setTypeface(null, Typeface.BOLD)
                Log.d("My", "Adpater: ${item.name} ustawiono warunek 3")
            }

            if(item.quantity % 1 != 0.0)
                textView_q_u.text = "${item.quantity} ${item.unit}"
            else
                textView_q_u.text = "${decimalFormat.format(item.quantity)} ${item.unit}"
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ProductListViewHolder, position: Int) {
        val current = items[position]
        holder.bind(current)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductListViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.recyclerview_product, parent, false)

        val holder = ProductListViewHolder(view)
        holder.itemView.setOnClickListener {
            listener.handleItemClicked(items[holder.adapterPosition])
        }

        holder.itemView.setOnLongClickListener {
            listener.handleItemLongClicked(items[holder.adapterPosition])
            true
        }

        return holder
    }

    fun updateData(data: List<ProductsEntity>) {
        Log.d("My", "Adapter: update all")
        items = ArrayList(data)
        notifyDataSetChanged()
    }

    fun delete(productsEntity: ProductsEntity) {
        items.remove(productsEntity)
        notifyDataSetChanged()
    }
}
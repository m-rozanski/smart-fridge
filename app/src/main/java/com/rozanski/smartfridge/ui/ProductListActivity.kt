package com.rozanski.smartfridge.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rozanski.smartfridge.R
import com.rozanski.smartfridge.REQUEST_CODE_ADD_ACTIVITY
import com.rozanski.smartfridge.REQUEST_CODE_EDIT_ACTIVITY
import com.rozanski.smartfridge.database.ProductsEntity
import com.rozanski.smartfridge.date2Long
import com.rozanski.smartfridge.di.component.DaggerProductListActivityComponent
import com.rozanski.smartfridge.di.module.PresenterModule
import kotlinx.android.synthetic.main.activity_product_list_activity.*
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class ProductListActivity : AppCompatActivity(), ProductListContract.View,
    ProductListAdapter.MyClickListener {
    @Inject
    lateinit var presenter: ProductListPresenter
    private lateinit var adapter: ProductListAdapter
    lateinit var item2Change: ProductsEntity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list_activity)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.title = getString(R.string.app_name)
        adapter = ProductListAdapter(ArrayList(), this)
        injectDependency()
        presenter.attachView(this)
        presenter.setup()
        //insertDummyData()
        initView()
        presenter.loadData()

    }

    private fun insertDummyData() {
        presenter.add("pierwszy", date2Long(GregorianCalendar(2019, 10, 28).time), 2.0, "opak")
        presenter.add("drugi", date2Long(GregorianCalendar(2019, 11, 15).time), 200.0, "ml")
        presenter.add("trzeci", date2Long(GregorianCalendar(2019, 10, 30).time), 0.5, "l")
        presenter.add("czwarty", date2Long(GregorianCalendar(2019, 10, 29).time), 7.0, "plastry")

    }

    private fun initView() {
        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.adapter = adapter
    }

    private fun injectDependency() {
        DaggerProductListActivityComponent.builder()
            .presenterModule(PresenterModule(this))
            .build()
            .inject(this)
    }

    override fun showMessage(msg: Int) {
        Toast.makeText(this, getString(msg), Toast.LENGTH_SHORT).show()
    }

    override fun updateData(data: List<ProductsEntity>) {
        adapter.updateData(data)
    }

    override fun handleItemClicked(productsEntity: ProductsEntity) {
        item2Change = productsEntity
        val intent = Intent(this, AddOrEditProductActivity::class.java)
        intent.putExtra("NAME", item2Change.name)
        intent.putExtra("DATE", item2Change.date)
        intent.putExtra("QUANTITY", item2Change.quantity)
        intent.putExtra("UNIT", item2Change.unit)
        startActivityForResult(intent, REQUEST_CODE_EDIT_ACTIVITY)
    }

    override fun handleItemLongClicked(productsEntity: ProductsEntity) {
        AlertDialog.Builder(this)
            .setMessage(getString(R.string.deleteItem))
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                dialog.dismiss()
                adapter.delete(productsEntity)
                presenter.delete(productsEntity)
            }.setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.cancel()
            }.create().show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar_add, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_add_product) {
            val intent = Intent(this, AddOrEditProductActivity::class.java)
            startActivityForResult(intent, REQUEST_CODE_ADD_ACTIVITY)
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data != null) {
            when (requestCode) {
                REQUEST_CODE_ADD_ACTIVITY -> {
                    val e = data.extras
                    val name: String = e["NAME"].toString()
                    val date: Long = e["DATE"].toString().toLong()
                    val quant: Double = e["QUANTITY"].toString().toDouble()
                    val unit: String = e["UNIT"].toString()
                    presenter.add(name, date, quant, unit)
                }

                REQUEST_CODE_EDIT_ACTIVITY -> {
                    val e = data.extras
                    val name = e["NAME"].toString()
                    val date: Long = e["DATE"].toString().toLong()
                    val quant: Double = e["QUANTITY"].toString().toDouble()
                    val unit: String = e["UNIT"].toString()
                    val updatedItem = ProductsEntity(item2Change.id, name, date, quant, unit)
                    presenter.update(updatedItem)
                }

                else -> Toast.makeText(this, "ups", Toast.LENGTH_SHORT).show()
            }
        }
    }
}

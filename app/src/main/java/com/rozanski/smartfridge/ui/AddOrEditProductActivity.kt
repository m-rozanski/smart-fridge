package com.rozanski.smartfridge.ui

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.rozanski.smartfridge.R
import com.rozanski.smartfridge.date2Long
import com.rozanski.smartfridge.long2Date
import kotlinx.android.synthetic.main.activity_add_product.*
import java.text.SimpleDateFormat
import java.util.*

class AddOrEditProductActivity : AppCompatActivity() {
    val c = Calendar.getInstance()
    val year = c.get(Calendar.YEAR)
    val month = c.get(Calendar.MONTH)
    val day = c.get(Calendar.DAY_OF_MONTH)
    var date: Long = c.timeInMillis
    lateinit var dateFormat: SimpleDateFormat

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_product)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.title = resources.getString(R.string.addProduct)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        dateFormat = SimpleDateFormat(resources.getString(R.string.dateFormat))

        val e = intent.extras
        if (e != null) {
            val name: String = e["NAME"].toString()
            date = e["DATE"].toString().toLong()
            val quant: String = e["QUANTITY"].toString()
            val unit: String = e["UNIT"].toString()

            text_name.setText(name)
            text_quantity.setText(quant)
            text_unit.setText(unit)
            textView_date.text = dateFormat.format(long2Date(date))
        } else {
            textView_date.text = dateFormat.format(c.time)
        }


        btn_date.setOnClickListener {
            chooseDate()
        }
    }

    private fun chooseDate() {
        val dpd =
            DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    textView_date.setText("$dayOfMonth/${monthOfYear + 1}/$year")
                    date = date2Long(GregorianCalendar(year, monthOfYear, dayOfMonth).time)
                    Log.d("My", "$date")
                },
                year,
                month,
                day
            )

        dpd.show()
    }

    private fun doneClick() {
        val name = text_name.text
        val quant = text_quantity.text
        val unit = text_unit.text

        if (name.isNullOrBlank() || quant.isNullOrBlank() || unit.isNullOrBlank()) {
            Toast.makeText(this, resources.getString(R.string.fill), Toast.LENGTH_LONG).show()
        } else {
            val resultIntent: Intent = Intent()
            resultIntent.putExtra("NAME", name)
            resultIntent.putExtra("DATE", date)
            resultIntent.putExtra("QUANTITY", quant)
            resultIntent.putExtra("UNIT", unit)

            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar_done, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_done) {
            doneClick()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

}
package com.rozanski.smartfridge.ui

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.rozanski.smartfridge.R
import com.rozanski.smartfridge.database.ProductsEntity
import com.rozanski.smartfridge.database.ProductsRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ProductListPresenter : AppCompatActivity(), ProductListContract.Presenter {
    private var view: ProductListContract.View? = null
    private val db = ProductsRepository()
    private val disposable = CompositeDisposable()

    fun setup() {
        db.setup()
    }

    override fun loadData() {
        disposable.add(
            db.getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view?.updateData(it)
                }, {
                    showMessage(R.string.loadingDatabaseError)
                    Log.d("My", it.message)
                })
        )
    }

    override fun update(item: ProductsEntity) {
        disposable.add(
            db.update(item)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                }, {
                    showMessage(R.string.updatingItemToDatabaseError)
                    Log.d("My", it.message)
                })
        )
    }

    override fun add(name: String, date: Long, quantity: Double, unit: String) {
        val item = ProductsEntity(0, name, date, quantity, unit)
        disposable.add(
            db.insert(item)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                }, {
                    showMessage(R.string.addingItemToDatabaseError)
                    Log.d("My", it.message)
                })
        )
    }

    override fun delete(productsEntity: ProductsEntity) {
        disposable.add(
            db.delete(productsEntity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                }, {
                    showMessage(R.string.deletingItemFromDatabaseError)
                    Log.d("My", it.message)
                })
        )
    }

    override fun attachView(v: ProductListContract.View) {
        view = v
    }

    override fun detachView() {
        view = null
    }

    override fun showMessage(msg: Int) {
        view?.showMessage(msg)
    }

}
package com.rozanski.smartfridge.ui

import com.rozanski.smartfridge.database.ProductsEntity

interface ProductListContract {

    interface View {
        fun updateData(data: List<ProductsEntity>)
        fun showMessage(msg: Int)
    }

    interface Presenter {
        fun loadData()
        fun update(item: ProductsEntity)
        fun add(name: String, date: Long, quantity: Double, unit: String)
        fun delete(productsEntity: ProductsEntity)
        fun showMessage(msg: Int)

        fun attachView(v: View)
        fun detachView()
    }
}
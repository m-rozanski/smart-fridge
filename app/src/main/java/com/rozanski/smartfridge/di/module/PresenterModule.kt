package com.rozanski.smartfridge.di.module

import android.app.Activity
import com.rozanski.smartfridge.ui.ProductListPresenter
import dagger.Module
import dagger.Provides

@Module
class PresenterModule(private var activity: Activity) {

    @Provides
    fun provideActivity(): Activity {
        return activity
    }

    @Provides
    fun providePresenter(): ProductListPresenter {
        return ProductListPresenter()
    }
}
package com.rozanski.smartfridge.di.component

import com.rozanski.smartfridge.di.module.PresenterModule
import com.rozanski.smartfridge.ui.ProductListActivity
import dagger.Component

@Component(modules = [PresenterModule::class])
interface ProductListActivityComponent {

    fun inject(productListActivity: ProductListActivity)
}
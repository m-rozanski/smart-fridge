package com.rozanski.smartfridge.di.module

import android.app.Application
import com.rozanski.smartfridge.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val app: App) {

    @Provides
    @Singleton
    fun provideApplication(): Application {
        return app
    }
}
package com.rozanski.smartfridge.di.module

import androidx.room.Room
import com.rozanski.smartfridge.App
import com.rozanski.smartfridge.DATABASE_NAME
import com.rozanski.smartfridge.database.ProductsDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(): ProductsDatabase {
        return Room.databaseBuilder(App.instance, ProductsDatabase::class.java, DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }
}
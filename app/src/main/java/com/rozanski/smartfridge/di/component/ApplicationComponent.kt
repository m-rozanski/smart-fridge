package com.rozanski.smartfridge.di.component

import com.rozanski.smartfridge.App
import com.rozanski.smartfridge.di.module.ApplicationModule
import dagger.Component

@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(application: App)

}
package com.rozanski.smartfridge.di.component

import com.rozanski.smartfridge.database.ProductsRepository
import com.rozanski.smartfridge.di.module.DatabaseModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DatabaseModule::class])
interface ProductsRepositoryComponent {
    fun inject(productsRepository: ProductsRepository)

    @Component.Builder
    interface Builder {
        fun build(): ProductsRepositoryComponent
        fun databaseModule(databaseModule: DatabaseModule): Builder
    }
}
package com.rozanski.smartfridge

import java.text.DecimalFormat
import java.util.*

const val REQUEST_CODE_ADD_ACTIVITY = 1
const val REQUEST_CODE_EDIT_ACTIVITY = 2

const val PRODUCTS_TABLE_NAME = "products"
const val DATABASE_NAME = "products_database"
val decimalFormat = DecimalFormat("0.#")

fun date2Long(date: Date): Long {
    return date.time
}

fun long2Date(date: Long): Date {
    return Date(date)
}

package com.rozanski.smartfridge

import android.app.Application
import com.facebook.stetho.Stetho
import com.rozanski.smartfridge.di.component.ApplicationComponent
import com.rozanski.smartfridge.di.component.DaggerApplicationComponent
import com.rozanski.smartfridge.di.module.ApplicationModule

class App : Application() {

    lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        Stetho.initializeWithDefaults(this)

        instance = this
        setup()
    }

    private fun setup() {
        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this)).build()
        component.inject(this)
    }

    fun getApplicationComponent(): ApplicationComponent {
        return component
    }

    companion object {
        lateinit var instance: App private set
        //var prefs: Prefs? = null
    }
}
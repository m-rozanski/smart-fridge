Smart Fridge
====================

Smart Fridge allows you to easily organize your food. Thanks to it, you'll never forget the expiration date of your products. Added products are displayed as a list on the main screen, and those with the nearest best before date are at the top. In addition, you can add the amount of product you have, so you can quickly check if something is missing while shopping.


---

Functionality
====================

* Add product to the list including expiration date and quantity
* Click on product to edit
* Long click on product to delete


---

Technologies used in this project:
====================

* MVP
* [RxJava 2][rxjava]
* [Dagger 2][dagger]
* [Room database][room]
* [RecyclerView][recycler]
* [CardView][card]


[rxjava]: https://github.com/ReactiveX/RxJava
[dagger]: https://github.com/google/dagger
[room]: https://developer.android.com/jetpack/androidx/releases/room
[recycler]: https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView
[card]: https://developer.android.com/reference/kotlin/androidx/cardview/widget/CardView